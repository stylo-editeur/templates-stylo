<?xml version="1.0" encoding="UTF-8"?>
<!-- Dernière modification : 2022-03-05.
XSLT développée pour exporter vers TEI Métopes des articles stylo convertis en TEI par pandoc.
Développée par Yves Marcoux.
Version préliminaire, 2020-12-18.
Licence en préparation - Tous droits réservés.
-->
<xsl:stylesheet version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0"
  xmlns="http://www.tei-c.org/ns/1.0" xmlns:stylo="http://stylo.huma-num.fr/ns/1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="#all">

  <xsl:output method="xml" version="1.0" indent="yes" encoding="UTF-8"
    omit-xml-declaration="no"/>

  <xsl:variable name="styloData" select="/TEI/teiHeader/xenoData/stylo:stylo-metadata"/>
  <xsl:variable name="biblioLikeDivs" select="/TEI/text/body/div[@xml:id='bibliographie'
    or contains(@xml:id, 'citation') or contains(@xml:id, 'graphie')]" />
  <xsl:variable name="moveToBack" select="$biblioLikeDivs |
    /TEI/text/body/div[contains(@xml:id, 'biographi')]"/>

  <xsl:template match="/">
    <xsl:comment>Ceci est le document output.</xsl:comment>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="teiHeader">
    <teiHeader>
      <xsl:apply-templates select="fileDesc"/>
<!--
      <encodingDesc>
        <tagsDecl>
          <rendition scheme="css" xml:id="none">color:black;</rendition>
        </tagsDecl>
      </encodingDesc>
-->
      <profileDesc>

        <langUsage>
          <xsl:for-each select="//@xml:lang">
            <language ident="{.}"/>
          </xsl:for-each>
        </langUsage>

        <textClass>
          <xsl:variable name="myChunk" select=
            "$styloData/stylo:kwLists/stylo:kwList[stylo:lang = 'fr']"/>
          <xsl:if test="$myChunk">
            <keywords scheme="keyword" xml:lang="fr">
              <list>
                <xsl:for-each select="tokenize($myChunk/stylo:list, ',')">
                  <item><xsl:value-of select="normalize-space()"/></item>
                </xsl:for-each>
              </list>
            </keywords>
          </xsl:if>
          <xsl:variable name="myChunk" select=
            "$styloData/stylo:kwLists/stylo:kwList[stylo:lang = 'en']"/>
          <xsl:if test="$myChunk">
            <keywords scheme="keyword" xml:lang="en">
              <list>
                <xsl:for-each select="tokenize($myChunk/stylo:list, ',')">
                  <item><xsl:value-of select="normalize-space()"/></item>
                </xsl:for-each>
              </list>
            </keywords>
          </xsl:if>
          <xsl:if test="normalize-space($styloData/stylo:controlledKeywords)">
            <keywords scheme="Rameau" xml:lang="fr">
              <list>
                <xsl:for-each
                  select="$styloData/stylo:controlledKeywords/stylo:controlledKeyword">
                  <item source="{stylo:uriRameau}">
                    <xsl:value-of select="stylo:label"/>
                  </item>
                </xsl:for-each>
              </list>
            </keywords>
          </xsl:if>
        </textClass>

      </profileDesc>

    </teiHeader>
  </xsl:template>

  <xsl:template match="titleStmt">
    <titleStmt>
      <title type="main">
        <xsl:apply-templates select="$styloData/stylo:title_f/node()"/>
      </title>
      <xsl:if test="$styloData/subtitle_f">
        <title type="sub">
          <xsl:apply-templates select="$styloData/stylo:subtitle_f/node()"/>
        </title>
      </xsl:if>
      <xsl:apply-templates select="$styloData/stylo:authors/stylo:author"/>
      <xsl:apply-templates select="$styloData/stylo:translators/stylo:translator"/>
    </titleStmt>
    <editionStmt>
      <edition>
        <date>
          <xsl:value-of select="translate($styloData/stylo:date, '/', '-')"/>
        </date>
      </edition>
    </editionStmt>
  </xsl:template>

  <xsl:template match="publicationStmt">
    <publicationStmt>
      <xsl:copy-of select="publisher" />
    </publicationStmt>
  </xsl:template>

  <xsl:template match="sourceDesc">
    <sourceDesc>
      <p>Version métopes : "nouveau schéma" - mars 2022</p>
      <p>Conversion XSLT Stylo → Métopes <xsl:value-of
          select="translate(substring(string(current-dateTime()), 1, 16), 'T', ' ')"/></p>
      <xsl:comment>XSLT développée par Yves Marcoux</xsl:comment>
    </sourceDesc>
  </xsl:template>

  <xsl:template match="stylo:translator">
    <xsl:if test="normalize-space()">
      <editor role="trl">
        <persName>
          <forename><xsl:apply-templates select="stylo:forname" /></forename>
          <surname><xsl:apply-templates select="stylo:surname"/></surname>
        </persName>
      </editor>
    </xsl:if>
  </xsl:template>

  <xsl:template match="stylo:forname | stylo:surname">
    <xsl:apply-templates select="@* | node()" />
  </xsl:template>

  <xsl:template match="stylo:author">
    <xsl:if test="normalize-space()">
      <author role="aut">
        <persName>
          <forename><xsl:apply-templates select="stylo:forname" /></forename>
          <surname><xsl:apply-templates select="stylo:surname"/></surname>
        </persName>
        <affiliation>Affiliation à venir</affiliation>
        <xsl:if test="normalize-space(stylo:orcid)">
          <idno type="ORCID">
            <xsl:value-of select="stylo:orcid"/>
          </idno>
        </xsl:if>
        <xsl:if test="normalize-space(stylo:isni)">
          <idno type="ISNI">
            <xsl:value-of select="stylo:isni"/>
          </idno>
        </xsl:if>
        <xsl:if test="normalize-space(stylo:viaf)">
          <idno type="VIAF">
            <xsl:value-of select="stylo:viaf"/>
          </idno>
        </xsl:if>
        <xsl:if test="normalize-space(stylo:foaf)">
          <idno type="FOAF">
            <xsl:value-of select="stylo:foaf"/>
          </idno>
        </xsl:if>
      </author>
    </xsl:if>
  </xsl:template>

<!-- 2022-03-05 Ne sont plus appelés, car il n’y a plus de byline dans le front : -->
<!--
  <xsl:template match="stylo:author" mode="front">
    <docAuthor style="txt_auteur">
      <xsl:value-of select="stylo:forname"/>
      <xsl:text> </xsl:text>
      <hi rend="small-caps" style="typo_SC">
        <xsl:value-of select="stylo:surname"/>
      </hi>
      <affiliation>Affiliation à venir</affiliation>
    </docAuthor>
  </xsl:template>

  <xsl:template match="stylo:translator" mode="front">
    <xsl:if test="normalize-space()">
      <byline style="collaborateur">
        <xsl:value-of select="stylo:forname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="stylo:surname"/>
      </byline>
    </xsl:if>
  </xsl:template>
-->

  <xsl:template match="text">
    <text>
      <xsl:attribute name="xml:id" select="'text'"/>
      <front>
        <titlePage>
          <titlePart type="main">
            <xsl:apply-templates select="$styloData/stylo:title_f/node()"/>
          </titlePart>
          <xsl:if test="$styloData/subtitle_f">
            <titlePart type="sub">
              <xsl:apply-templates select="$styloData/stylo:subtitle_f/node()"/>
            </titlePart>
          </xsl:if>
<!--
          <byline>
            <xsl:apply-templates select="$styloData/stylo:authors/stylo:author"
              mode="front"/>
          </byline>
          <xsl:apply-templates select="$styloData/stylo:translators/stylo:translator"
            mode="front"/>
-->
        </titlePage>

        <xsl:variable name="myChunk" select=
          "$styloData/stylo:abstracts/stylo:abstract[stylo:lang = 'fr']"/>
        <xsl:if test="$myChunk">
          <div type="abstract" xml:lang="fr">
            <p><xsl:apply-templates select=
            "$myChunk/stylo:text_f/node()" /></p>
          </div>
        </xsl:if>
<!--
          <xsl:variable name="myChunk" select=
            "$styloData/stylo:kwLists/stylo:kwList[stylo:lang = 'fr']"/>
          <xsl:if test="$myChunk">
            <p style="txt_Motclef">Mots-clés :
              <xsl:value-of select="normalize-space(
                string-join(tokenize($myChunk/stylo:list, ','), ', ')
                )"/></p>
          </xsl:if>
-->

<!-- On n'inclut pas les descripteurs Rameau, car entre autres
ils contiennent parfois des virgules. Aussi, il pourrait y avoir des doublons avec
les autres mots-clés. -->
        <xsl:variable name="myChunk" select=
          "$styloData/stylo:abstracts/stylo:abstract[stylo:lang = 'en']"/>
        <xsl:if test="$myChunk">
          <div type="abstract" xml:lang="en">
            <p><xsl:apply-templates select=
            "$myChunk/stylo:text_f/node()" /></p>
          </div>
        </xsl:if>
<!--
          <xsl:variable name="myChunk" select=
            "$styloData/stylo:kwLists/stylo:kwList[stylo:lang = 'en']"/>
          <xsl:if test="$myChunk">
            <p style="txt_Motclef_italique" xml:lang="en"><xsl:comment
            >xml:lang="en" ajouté par souci de cohérence.</xsl:comment>Keywords:
              <xsl:value-of select="normalize-space(
                string-join(tokenize($myChunk/stylo:list, ','), ', ')
                )"/></p>
          </xsl:if>
-->
      </front>
      <xsl:apply-templates />
    </text>
  </xsl:template>

  <xsl:template match="stylo:*">
<!-- Template pour transférer certains éléments de stylo dans le ns TEI.
Filet de sécurité pour signaler des cas problèmes. -->
    <xsl:message select="concat(document-uri(/), ' ', local-name())"
      > élément inattendu de stylo !</xsl:message>
    <xsl:element name="{local-name()}" namespace="http://www.tei-c.org/ns/1.0">
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="@xml:id | @target[starts-with(., '#')]">
    <!-- Vérifier si c'est nécessaire, ou si c'est une erreur dans SP1436. -->
    <xsl:attribute name="{name()}" select="translate(., '/:', '__')"/>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="*">
<!-- Template par défaut qui ne fait que reproduire l'élément, tout en permettant d'agir
sur le contenu. Traite (entre autres ?) les p enfants de note, et parfois de quote. -->
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="$biblioLikeDivs//formula[
      @notation = 'TeX' and (. = '^{e}' or . = '^e')]">
    <hi rend="sup">e</hi>
<!-- Modifier plutôt le protocole de rédaction ? -->
  </xsl:template>

  <xsl:template match="hi | stylo:hi">
    <hi>
      <xsl:choose>
        <xsl:when test="@rendition = 'simple:italic'">
          <xsl:attribute name="rend" select="'italic'"/>
<!--          <xsl:attribute name="style" select="'typo_Italique'"/>-->
        </xsl:when>
        <xsl:when test="@rendition = 'simple:superscript'">
          <xsl:attribute name="rend" select="'sup'"/>
<!--          <xsl:attribute name="style" select="'typo_Exposant'"/>-->
        </xsl:when>
        <xsl:when test="@rendition = 'simple:bold'">
          <xsl:attribute name="rend" select="'bold'"/>
<!--          <xsl:attribute name="style" select="'typo_gras'"/>-->
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
    </hi>
  </xsl:template>

  <xsl:template match="quote[p]">
    <xsl:if test="*[name(.) != 'p']">
       <xsl:message
         select="concat(document-uri(/), ' Contenu inattendu dans un quote !')" />
    </xsl:if>
    <cit><quote>
<!-- Nous traitons séparément les cas où il y a exactement un seul p comme contenu
et les autres. -->
      <xsl:choose>
        <xsl:when test="(count(*) = 1) and not(text()[normalize-space()])">
<!-- On élimine le <p> comme intermédiaire sous le <quote> : -->
            <xsl:apply-templates select="p/@* | p/node()" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="@* | node()" />
        </xsl:otherwise>
      </xsl:choose>
    </quote></cit>
  </xsl:template>

  <xsl:template match="stylo:p">
<!-- On strip simplement les balises. SP1376 a des guillemets superflus autour des
résumés français. À vérifier que c'est nécessaire, peut-être un changement dans le
protocole de rédaction serait plus approprié. -->
    <xsl:apply-templates select="node()" />
  </xsl:template>

  <xsl:template match="quote | stylo:quote"
    >“<xsl:apply-templates select="@* | node()" />”</xsl:template>
<!-- Les quote[p] sont traités par un autre template. -->

  <xsl:template match="body">
    <body>
<!-- <div type="chapitre" xml:id="mainDiv"> -->
      <xsl:apply-templates select=
        "node() except $moveToBack" />
<!--</div>-->
    </body>
    <xsl:if test="$moveToBack">
<!-- SP1391 n'a pas de bibliographie. -->
      <back>
        <xsl:apply-templates select="$moveToBack" />
      </back>
    </xsl:if>
  </xsl:template>

  <xsl:template match="$biblioLikeDivs">
    <div type="bibliographie">
      <head><xsl:apply-templates select="head[1]/node()"/></head>
      <xsl:apply-templates select="node() except head[1]" />
    </div>
  </xsl:template>

  <xsl:template match="div except $biblioLikeDivs">
    <xsl:variable name="niveau" select="string(count(ancestor-or-self::div))" />
    <div type="section{$niveau}">
      <xsl:apply-templates select="@xml:id" />
      <xsl:apply-templates select="node()" />
    </div>    
  </xsl:template>

  <xsl:template match="head">
<!-- 2022-03-04 <xsl:variable name="niveau" select="string(count(ancestor-or-self::div))" />-->
    <head>
      <xsl:apply-templates select="node()" />
    </head>
  </xsl:template>

  <xsl:template match="p[not(parent::quote | parent::note)]">
    <p>
<!-- style="txt_Normal" retiré 2022-03-04 -->
      <xsl:apply-templates select="node()" />
    </p>
  </xsl:template>

  <xsl:template match="p[figure and (count(*) = 1)]" priority="2">
    <xsl:variable name="numFig" select="1 + count(
      if (. intersect $moveToBack//figure) then
      (//figure except $moveToBack//figure) |
      ($moveToBack//figure intersect preceding::figure)
      else
      (preceding::figure except
      $moveToBack//figure)
      )" />
<!-- Couvre les cas où il y a des figures dans des sections à déplacer en back. -->
    <figure xml:id="fig{$numFig}">
<!-- Vérifier s'il faut le numFig sur 2 chiffres. -->
      <xsl:copy-of select="figure/graphic" />
      <head>Figure <xsl:value-of 
        select="$numFig"/> – <xsl:apply-templates
          select="figure/head/node()" />
      </head>
    </figure>
  </xsl:template>

  <xsl:template match="$biblioLikeDivs/p" priority="2">
    <bibl>
<!-- style="txt_Bibliographie" -->
      <xsl:apply-templates select="@xml:id" />
      <xsl:apply-templates select="node()" />
    </bibl>
  </xsl:template>

  <xsl:template match="note">
    <xsl:variable name="numNote" select="1 + count(
      if (. intersect $moveToBack//note) then
        (//note except $moveToBack//note) |
        ($moveToBack//note intersect preceding::note)
      else
      (preceding::note except
      $moveToBack//note)
      )" />
<!-- Couvre les cas où il y a des notes dans des sections à déplacer en back. -->
    <note n="{$numNote}" place="foot"
      xml:id="ftn{$numNote}">
<!-- style="txt_Note" type="standard" retirés 2022-03-04 -->
      <xsl:apply-templates select="node()" />
    </note>
  </xsl:template>

  <xsl:template match="table">
    <xsl:variable name="numTable" select="1 + count(
      if (. intersect $moveToBack//table) then
      (//table except $moveToBack//table) |
      ($moveToBack//table intersect preceding::table)
      else
      (preceding::table except
      $moveToBack//table)
      )" />
<!-- Couvre les cas où il y a des tables dans des sections à déplacer en back. -->
    <table xml:id="Table{$numTable}"
      cols="{count(row[1]/cell)}"
      rows="{count(row)}">
      <xsl:apply-templates select="node()" />
    </table>
  </xsl:template>

  <xsl:template match="cell/p" priority="2">
<!-- On strip simplement les balises du <p>. -->
    <xsl:apply-templates select="node()" />
  </xsl:template>

</xsl:stylesheet>
